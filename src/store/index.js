import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    headersLogin: { 'Content-type': 'application/json' },
  },
  mutations: {
  },
  actions: {
  },
  modules: {
  }
})
